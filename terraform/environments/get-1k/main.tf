terraform {
  backend "gcs" {
    bucket  = "jm-get"
    prefix = "jm-get-1k-tf-state"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
    }
  }
}

provider "google" {
  project = var.project
  region  = var.region
  zone    = var.zone
}
