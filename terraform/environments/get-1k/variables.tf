variable "prefix" {
  default = "jm-get-1k"
}

variable "project" {
  default = "jmartinez-99e5a214"
}

variable "region" {
  default = "australia-southeast1"
}

variable "zone" {
  default = "australia-southeast1-a"
}

variable "external_ip" {
  default = "34.87.254.155"
}
